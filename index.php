<?php
    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');
    

    $animal = new Animal("shaun");
    echo "Name : ".$animal->name."<br>";
    echo "legs : ".$animal->legs."<br>";
    echo "cold blooded : ".$animal->cold_blooded."<br><br>";


    $kodok = new Frog("buduk");
    echo "Name : ".$kodok->name ."<br>";
    echo "legs : ".$kodok->legs ."<br>";
    echo "cold blooded : ".$kodok->cold_blooded ."<br>";
    echo $kodok ->jump("Hop Hop") ."<br><br>";
    

    $Sungokong = new Ape("kera sakti");
    echo "Name : ".$Sungokong->name."<br>";
    echo "legs : ".$Sungokong->legs."<br>";
    echo "cold blooded : ".$Sungokong->cold_blooded."<br>";
    echo $Sungokong->yell("Auooo");



?>